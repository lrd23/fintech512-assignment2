
var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => {
    document.getElementById("btSubmit").addEventListener('click', handleClick)
    const selectElement = document.querySelector('#stocks-select');
    selectElement.addEventListener('change', (event) => {
        console.log(event.target)
        let stockSymbol = event.target.value;
        makeplot(stockSymbol);
    });
});

function handleClick(event) {
    bootbox.confirm({
        message: 'Would you like to submit the page?',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            console.log('This was logged in the callback: ' + result);
            if (result) {
                bootbox.alert("Form Submitted")
            }
            else {
                bootbox.alert("Action Cancelled")
            }
        }
    });
};

function makeplot(stockSymbol) {
    console.log("makeplot: start")
    let url = (stockSymbol + ".csv")
    fetch(url)
        .then((response) => response.text()) /* asynchronous */
        .catch(error => {
            alert(error)
        })
        .then((text) => {
            console.log("csv: start")
            csv().fromString(text).then((result) => { processData(result, stockSymbol)})  /* asynchronous */
            console.log("csv: end")
        })
        console.log("makeplot: end")
};


function processData(data, stockSymbol) {
    console.log("processData: start")
    let x = [], y = []

    for (let i = 0; i < data.length; i++) {
        row = data[i];
        x.push(row['Date']);
        y.push(row['Close']);
    }
    makePlotly( x, y, stockSymbol);
    console.log("processData: end")
}

var title = { "AAPL": "Apple", "GME": "Gamestop", "TSLA": "Tesla", "SPY": "S & P 500" }

function makePlotly(x, y, stockSymbol) {
    console.log("makePlotly: start")
    var traces = [{
        x: x,
        y: y
    }];
    var layout = { title: title[stockSymbol] + " Stock Price History" }

    myDiv = document.getElementById('myDiv');
    Plotly.newPlot(myDiv, traces, layout);
    console.log("makePlotly: end")
};